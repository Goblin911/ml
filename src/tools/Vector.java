package tools;

import java.util.HashMap;
import java.util.Map;

public class Vector {

	HashMap<Integer, Double> a;

	public Vector() {
		a = new HashMap<>();
	}

	public void set(int w, double v) {
		a.put(w, v);
	}

	public double dotMul(Vector t) {
		double res = 0;
		for (Map.Entry<Integer, Double> i : t.a.entrySet()) {
			if (a.containsKey(i.getKey())) {
				res += i.getValue() * a.get(i.getKey());
			}
		}
		return res;
	}

	public double sum() {
		double ans = 0;
		for (Map.Entry<Integer, Double> i : a.entrySet()) {
			ans += i.getValue();
		}
		return ans;
	}

	public double sqrNormF() {
		double ans = 0;
		for (Map.Entry<Integer, Double> i : a.entrySet()) {
			ans += i.getValue() * i.getValue();
		}
		return ans;
	}

	public double get(int t) {
		if (a.containsKey(t))
			return a.get(t);
		else
			return 0;
	}

}
