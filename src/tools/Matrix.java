package tools;

import java.util.*;

public class Matrix {

	public class Node {
		public int x, y;

		Node(int x, int y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public int hashCode() {
			return x * 100007 + y;
		}

		@Override
		public boolean equals(Object p) {
			if (p instanceof Node) {
				Node tmp = (Node) p;
				return tmp.x == x && tmp.y == y;
			} else
				return false;
		}
	}

	HashMap<Node, Double> a;

	public double[] normalize(int m) {
		double[] u = new double[m];
		int[] num = new int[m];
		for (Map.Entry<Node, Double> i : a.entrySet()) {
			Node t = i.getKey();
			u[t.y] += i.getValue();
			num[t.y]++;
		}
		for (int i = 0; i < m; i++)
			if (num[i] > 0)
				u[i] /= num[i];
			else u[i] = 0;
		for (Map.Entry<Node, Double> i : a.entrySet()) {
			Node t = i.getKey();
			double tmp = i.getValue() - u[t.y];
			a.put(t, tmp);
		}
		return u;
	}

	public Matrix(data.Node[] data) {
		a = new HashMap<>();
		for (int i = 0; i < data.length; i++) {
			a.put(new Node(data[i].userID, data[i].movieID),
					(double) data[i].rate);
		}
	}

	public Matrix() {
		a = new HashMap<>();
	}

	public void add(Matrix t) {
		for (Map.Entry<Node, Double> i : a.entrySet()) {
			if (a.containsKey(i.getKey())) {
				Node w = i.getKey();
				double r = a.get(w);
				r += i.getValue();
				a.put(w, r);
			} else {
				Node w = i.getKey();
				double r = a.get(w);
				a.put(w, r);
			}
		}
	}

	public void mines(Matrix t) {
		for (Map.Entry<Node, Double> i : a.entrySet()) {
			if (a.containsKey(i.getKey())) {
				Node w = i.getKey();
				double r = a.get(w);
				r -= i.getValue();
				a.put(w, r);
			} else {
				Node w = i.getKey();
				double r = -a.get(w);
				a.put(w, r);
			}
		}
	}

	public void mul(Matrix t) {
		HashMap<Node, Double> res = new HashMap<>();
		for (Map.Entry<Node, Double> i : a.entrySet()) {
			for (Map.Entry<Node, Double> j : t.a.entrySet())
				if (i.getKey().y == j.getKey().x) {
					int x = i.getKey().x;
					int y = j.getKey().y;
					Node p = new Node(x, y);
					if (res.containsKey(p)) {
						double v = res.get(p);
						v += i.getValue() * j.getValue();
						res.put(p, v);
					} else {
						double v = i.getValue() * j.getValue();
						res.put(p, v);
					}
				}
		}
		a = res;
	}

	public void tranverse() {
		HashMap<Node, Double> res = new HashMap<>();
		for (Map.Entry<Node, Double> i : a.entrySet()) {
			Node t = i.getKey();
			Node p = new Node(t.y, t.x);
			res.put(p, i.getValue());
		}
		a = res;
	}

	public void dotMul() {
		for (Map.Entry<Node, Double> i : a.entrySet()) {
			Node t = i.getKey();
			a.put(t, i.getValue() * i.getValue());
		}
	}

	public double sum() {
		double ans = 0;
		for (Map.Entry<Node, Double> i : a.entrySet()) {
			ans += i.getValue();
		}
		return ans;
	}

	public double normF() {
		double ans = 0;
		for (Map.Entry<Node, Double> i : a.entrySet()) {
			ans += i.getValue() * i.getValue();
		}
		return Math.sqrt(ans);
	}

	public Set<Map.Entry<Node, Double>> getEntrySet() {
		return a.entrySet();
	}

	@SuppressWarnings("unchecked")
	public Matrix copy() {
		Matrix res = new Matrix();
		res.a = (HashMap<Node, Double>) a.clone();
		return res;
	}

}
