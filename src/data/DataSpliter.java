package data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

public class DataSpliter {
	Node[] data;
	PrintStream outTrain, outVal, outTest;
	int tr, val, test, all;
	String trainName, valName, testName;

	public DataSpliter(Node[] data) {
		this.data = data;
		tr = 8;
		val = 0;
		test = 2;
		all = tr + val + test;
		trainName = "train.dat";
		valName = "validation.dat";
		testName = "test.dat";
	}

	void split(String a, String b, double rate) {
		File fileA = null;
		File fileB = null;
		fileA = new File(a);
		fileB = new File(b);
		int n = data.length;
		int k = (int) (n * rate);
		try {
			@SuppressWarnings("resource")
			PrintStream out = new PrintStream(fileA);
			for (int i = 0; i < k; i++)
				out.println(data[i].toString());
			out = new PrintStream(fileB);
			for (int i = k; i < n; i++)
				out.println(data[i].toString());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

	public Vector<Vector<Node>> splitTrainVal() {
		Vector<Node> user = new Vector<Node>();
		Vector<Vector<Node>> tmp = new Vector<>();
		for (int i = 0; i < 5; i++)
			tmp.add(new Vector<Node>());
		for (int i = 0; i < data.length; i++) {
			if (i != 0 && data[i].userID != data[i - 1].userID) {
				int n = user.size();
				int a = n / 5;
				if (a > 0) {
					Collections.shuffle(user);
					for (int j = 0; j < n; j++)
						if (j / a < 5) {
							Node u = user.get(j);
							tmp.get(j / a).add(u);
						}
				}
				user = new Vector<Node>();
			}
			user.add(data[i]);
		}
		return tmp;
	}

	// 6:2:2 for train:val:test
	public void split() {
		Arrays.sort(data, Node.sortByUser());
		try {
			outTrain = new PrintStream(new File(trainName));
			outVal = new PrintStream(new File(valName));
			outTest = new PrintStream(new File(testName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		}

		Vector<Node> user = new Vector<Node>();
		for (int i = 0; i < data.length; i++) {
			if (i != 0 && data[i].userID != data[i - 1].userID) {
				Collections.shuffle(user);
				int n = user.size();
				int a = n * tr / all;
				int b = n * val / all + a;
				for (int j = 0; j < n; j++) {
					Node u = user.get(j);
					if (j <= a)
						outTrain.println(u.toString());
					else if (j <= b)
						outVal.println(u.toString());
					else
						outTest.println(u.toString());
				}
				user = new Vector<Node>();
			}
			user.add(data[i]);
		}
		outTrain.close();
		outVal.close();
		outTest.close();
	}

	public static void main(String[] args) {
		InputData in = new NipsInputStream("data/training_set");
//		InputData in = new MovieLensInput("data/ratings.dat");
		Node[] data = in.getData();
		DataSpliter spliter = new DataSpliter(data);
		spliter.split();
		try {
			@SuppressWarnings("resource")
			PrintStream out = new PrintStream(new File("info.ini"));
			out.println("user_number: " + in.userNum);
			out.println("item_number: " + in.itemNum);
			out.println("data location: " + in.loc);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
