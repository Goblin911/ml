package data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintStream;
import java.io.StringWriter;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Vector;

public class NipsInputStream extends InputData {

	public NipsInputStream(String loc) {
		super(loc);
	}

	@Override
	public Node[] getData() {
		itemNum = 1000;
		Vector<Node> res = new Vector<>();
		for (int i = 1; i <= itemNum; i++) {
			@SuppressWarnings("resource")
			Formatter fmt = new Formatter();
			fmt.format("%07d", i);
			String l = loc + "/mv_" + fmt.toString() + ".txt";
			System.out.println(l);
			try {
				in = new BufferedReader(new FileReader(new File(l)));
				in.readLine();
				while (true) {
					String s = in.readLine();
					if (s == null)
						break;
					String[] a = s.split(",");
					int userID = Integer.parseInt(a[0]) - 1;
					int movieID = i - 1;
					int rate = Integer.parseInt(a[1]);
					Node t = new Node(userID, movieID, rate);
					res.add(t);
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			}
		}
		HashMap<Integer, Integer> mp = new HashMap<>();
		int num = 0;
		for (Node i : res) {
			if (mp.containsKey(i.userID)) {
				i.userID = mp.get(i.userID);
			} else {
				mp.put(i.userID, num);
				num++;
				i.userID = num++;
			}
		}

		this.userNum = num;
		System.out.println(userNum + " " + itemNum + "  " + res.size());
		Node[] rr = new Node[res.size()];
		return res.toArray(rr);
	}
}
