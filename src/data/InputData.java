package data;

import java.io.*;
import java.util.*;

public class InputData {
	String loc;
	BufferedReader in;
	public int userNum, itemNum; // 0 ~ xx-1

	public InputData(String loc) {
		this.loc = loc;
		try {
			in = new BufferedReader(new FileReader(new File(loc)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Node[] getData() {
		Vector<Node> res = new Vector<Node>();
		while (true) {
			try {
				String[] s = in.readLine().split(" ");
				int userID = Integer.parseInt(s[0]);
				int movieID = Integer.parseInt(s[1]);
				double rate = Double.parseDouble(s[2]);
				Node t = new Node(userID, movieID, rate);
				res.add(t);
			} catch (Exception e) {
				break;
			}
		}
		Node[] ans = new Node[res.size()];
		res.toArray(ans);
		try {
			in = new BufferedReader(new FileReader(new File("info.ini")));
			String[] s;
			s= in.readLine().split(" ");
			userNum = Integer.parseInt(s[1]);
			s = in.readLine().split(" ");
			itemNum = Integer.parseInt(s[1]);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		return ans;
	}
	
}
