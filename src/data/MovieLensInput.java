package data;

import java.util.Vector;
import static debug.Debug.*;
public class MovieLensInput extends InputData {

	public MovieLensInput(String loc) {
		super(loc);
	}

	@Override
	public Node[] getData() {
		Vector<Node> res = new Vector<Node>();
		userNum = 6040;
		itemNum = 3952;
		while (true) {
			try {
				String[] s = in.readLine().split("::");
				int userID = Integer.parseInt(s[0]) - 1;
				int movieID = Integer.parseInt(s[1]) - 1;
				int rate = Integer.parseInt(s[2]);
				Node t = new Node(userID, movieID, rate);
				res.add(t);
			} catch (Exception e) {
				if (debug) {
					e.printStackTrace();
				}
				if (info)
					System.out.println("read data over!");
				break;
			}
		}
		Node[] ans = new Node[res.size()];
		res.toArray(ans);
		return ans;
	}

}
