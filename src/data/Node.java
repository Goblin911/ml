package data;

import java.util.Comparator;

public class Node {
	public int movieID;
	public int userID;
	public double rate;

	Node(int userID, int movieID, double rate) {
		this.movieID = movieID;
		this.userID = userID;
		this.rate = rate;
	}

	@Override
	public String toString() {
		return userID + " " + movieID + " " + rate;
	}

	public static Comparator<Node> sortByUser() {
		return new Comparator<Node>() {
			public int compare(Node o1, Node o2) {
				return o1.userID - o2.userID;
			}
		};
	}
	
	public static Comparator<Node> sortByMovie() {
		return new Comparator<Node>() {
			public int compare(Node o1, Node o2) {
				return o1.movieID - o2.movieID;
			}
		};
	}
}
