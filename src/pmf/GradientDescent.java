package pmf;

import java.util.Map;
import java.util.Vector;

import com.sun.org.apache.xerces.internal.impl.dv.ValidatedInfo;

import tools.Matrix;
import tools.Matrix.Node;
import static debug.Debug.*;

public class GradientDescent {

	double lrate;
	Matrix r;
	double[][] u, v; // n * d && m * d
	double lamdaU, lamdaV;
	int n, m, d;
	double eps = 0.01;

	double sigmoid(double x) {
		return 1 / (1 + Math.exp(-x));
	}

	double costFunc() {
		double res = 0;
		for (Map.Entry<Node, Double> i : r.getEntrySet()) {
			double tmp = i.getValue();
			int x = i.getKey().x;
			int y = i.getKey().y;
			for (int j = 0; j < d; j++)
				tmp -= u[x][j] * v[y][j];
			res += tmp * tmp;
		}
		res /= 2;
		double tmp = 0;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < d; j++)
				tmp += u[i][j] * u[i][j];
		}
		tmp = tmp * lamdaU / 2;
		res += tmp;
		tmp = 0;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < d; j++)
				tmp += v[i][j] * v[i][j];
		}
		tmp = tmp * lamdaV / 2;
		res += tmp;
		return res;
	}

	// return d-dim vector
	double[][] gradU() {
		double[][] res = new double[n][d];
		for (Map.Entry<Node, Double> i : r.getEntrySet()) {
			int x = i.getKey().x;
			int y = i.getKey().y;
			double tt = i.getValue();
			for (int j = 0; j < d; j++) {
				tt -= u[x][j] * v[y][j];
			}
			for (int j = 0; j < d; j++)
				res[x][j] += tt * -v[y][j];
		}

		for (int i = 0; i < n; i++)
			for (int j = 0; j < d; j++) {
				res[i][j] += lamdaU * u[i][j];
			}
		return res;
	}

	double[] gradU(Map.Entry<Node, Double> i) {
		double[] res = new double[d];
		int x = i.getKey().x;
		int y = i.getKey().y;
		double tt = i.getValue();
		for (int j = 0; j < d; j++) {
			tt -= u[x][j] * v[y][j];
		}
		for (int j = 0; j < d; j++)
			res[j] += tt * -v[y][j];

		for (int j = 0; j < d; j++) {
			res[j] += lamdaU * u[x][j];
		}
		return res;
	}

	// return d-dim vector
	double[][] gradV() {
		double[][] res = new double[m][d];
		for (Map.Entry<Node, Double> i : r.getEntrySet()) {
			int x = i.getKey().x;
			int y = i.getKey().y;
			double tt = i.getValue();
			for (int j = 0; j < d; j++) {
				tt -= u[x][j] * v[y][j];
			}
			for (int j = 0; j < d; j++)
				res[y][j] += tt * -u[x][j];
		}
		for (int i = 0; i < m; i++)
			for (int j = 0; j < d; j++) {
				res[i][j] += lamdaV * v[i][j];
			}
		return res;
	}

	// return d-dim vector
	double[] gradV(Map.Entry<Node, Double> i) {
		double[] res = new double[d];
		int x = i.getKey().x;
		int y = i.getKey().y;
		double tt = i.getValue();
		for (int j = 0; j < d; j++) {
			tt -= u[x][j] * v[y][j];
		}
		for (int j = 0; j < d; j++)
			res[j] += tt * -u[x][j];

		for (int j = 0; j < d; j++) {
			res[j] += lamdaV * v[y][j];
		}
		return res;
	}

	void solve() {
		double cost = 1e9;
		double[][] lastU = new double[n][d];
		double[][] lastV = new double[m][d];
		double alpha = lrate;
		Vector<Double> his = new Vector<>();
		for (;;) {
			// for (Map.Entry<Node, Double> ii : r.getEntrySet()) {
			double now = costFunc();
			System.out.println(now + " " + cost);
			if (Math.abs(cost - now) < eps) {
				break;
			}
			if (now > cost) { // cost增加，减小alpha，并还原
				alpha /= 2;
				for (int i = 0; i < n; i++)
					for (int j = 0; j < d; j++)
						u[i][j] = lastU[i][j];
				for (int i = 0; i < m; i++)
					for (int j = 0; j < d; j++)
						v[i][j] = lastV[i][j];
			} else { // 变小了就增加alpha
				alpha *= 1.5;
				cost = now;
				his.add(cost);
			}
			// int x = ii.getKey().x;
			// int y = ii.getKey().y;
			// double[] uu = gradU(ii);
			// double[] vv = gradV(ii);
			// for (int j = 0; j < d; j++) {
			// u[x][j] -= alpha * uu[j];
			// v[y][j] -= alpha * vv[j];
			// }
			double[][] gU = gradU();
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < d; j++) {
					lastU[i][j] = u[i][j];
					u[i][j] -= alpha * gU[i][j];
				}
			}
			double[][] gV = gradV();
			for (int i = 0; i < m; i++) {
				for (int j = 0; j < d; j++) {
					lastV[i][j] = v[i][j];
					v[i][j] -= alpha * gV[i][j];
				}
				// }
			}
		}
		for (int i = 0; i < his.size(); i++)
			System.out.println(his.get(i));
	}

	public GradientDescent(int n, int m, int d, Matrix r, double lamdaU,
			double lamdaV, double alpha) {
		this.n = n;
		this.m = m;
		this.d = d;
		this.r = r;
		this.lamdaU = lamdaU;
		this.lamdaV = lamdaV;
		this.lrate = alpha;
		u = new double[n][d];
		v = new double[m][d];
		for (int i = 0; i < n; i++)
			for (int j = 0; j < d; j++)
				u[i][j] = (Math.random() - 0.5) / 100;
		for (int i = 0; i < m; i++)
			for (int j = 0; j < d; j++)
				v[i][j] = (Math.random() - 0.5) / 100;
	}

}
