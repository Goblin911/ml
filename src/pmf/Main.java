package pmf;

import java.util.Vector;

import tools.Matrix;
import data.*;

public class Main {
	int n, m, d;
	double lamdaU, lamdaV, alpha;
	Node[] allData;
	Vector<Vector<Node>> data;
	Node[] trainData, testData;

	void init() {
		InputData in = new InputData("train.dat");
		allData = in.getData();
		DataSpliter d = new DataSpliter(allData);
		data = d.splitTrainVal();
		n = in.userNum;
		m = in.itemNum;
	}

	Node[] getTrainData(int t) {
		int cap = 0;
		for (int i = 0; i < 5; i++)
			if (i != t)
				cap += data.get(i).size();
		Node[] res = new Node[cap];
		int itr = 0;
		for (int i = 0; i < 5; i++)
			if (i != t) {
				Vector<Node> v = data.get(i);
				for (Node j : v) {
					res[itr++] = j;
				}
			}
		return res;
	}

	Node[] getValData(int t) {
		int cap = data.get(t).size();
		Node[] res = new Node[cap];
		for (int i = 0; i < cap; i++)
			res[i] = data.get(t).get(i);
		return res;
	}
	
	Node[] getTestData() {
		InputData in = new InputData("test.dat");
		return in.getData();
	}

	double RMSD(double[][] u, double[][] v, double[] uu) {
		double res = 0;
		int n = testData.length;
		for (Node i : testData) {
			double tmp = 0;
			for (int j = 0; j < d; j++)
				tmp += u[i.userID][j] * v[i.movieID][j];
			tmp = tmp + uu[i.movieID];
			tmp -= i.rate;
			res += tmp * tmp;
		}
		res /= n;
		return Math.sqrt(res);
	}

	void train() {
		d = 2;
		lamdaV = lamdaU = 10;
		alpha = 0.1;
		double seme = 0;
		for (int i = 0; i < 1; i++) {
			trainData = getTrainData(-1);
			testData = getTestData();
			Matrix r = new Matrix(trainData);
			System.out.println("!!!!");
			double[] uu = r.normalize(m);
			GradientDescent g = new GradientDescent(n, m, d, r, lamdaU, lamdaV,
					alpha);
			g.solve();
			double tmp = RMSD(g.u, g.v, uu);
			seme += tmp;
		}
		System.out.println(seme);
	}

	Main() {
		init();
		train();
	}

	public static void main(String args[]) {
		new Main();
	}
}
